<?php

use Illuminate\Database\Seeder;
use App\User;
class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
    	$admin = User::create(['name' => 'admin', 'email'=>'admin@foo.com','password' => bcrypt('admin')]);
        // $this->call(UsersTableSeeder::class);
    }
}

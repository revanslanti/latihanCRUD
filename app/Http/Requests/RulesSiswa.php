<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RulesSiswa extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nama' => 'required|max:10|min:5',
            'kelas'=> 'required|max:10|min:5',
            'alamat'=> 'required|max:10|min:5'

        ];
    }
}

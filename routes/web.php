<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Auth::routes();

Route::group(['middleware' => ['web','auth']], function(){

	Route::get('/', function () {
    return view('welcome');
	});

	Route::get('/home', function () {
    if (Auth::user()->admin == 0){
    	return view('/home');
    }else{
    	$users['users'] = \App\user::all();
    	return view('welcome', $users);
    }
	});
});

Route::get('/home', 'HomeController@index')->name('home');

Route::get('siswa/create', 'SiswaController@create');
Route::get('siswa', 'SiswaController@index');
Route::get('siswa/edit/{id}', 'SiswaController@edit');
Route::put('siswa/update/{id}', 'SiswaController@update');

Route::get('siswa/delete/{id}', 'SiswaController@destroy');
Route::post('siswa', 'SiswaController@store');


Route::get('guru/create', 'GuruController@create');
Route::get('guru', 'GuruController@index');
Route::post('guru', 'GuruController@store');
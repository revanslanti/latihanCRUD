@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">input Siswa</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    <div class="row">
                        <div class="col-md-12">
                            <form action="/insert" method="POST">
                                <table>
                                    <tr>
                                        <td>nama:</td>
                                        <td><input type="text" name="nama" class="input"></td>
                                    </tr>
                                    <tr>
                                        <td>Kelas:</td>
                                        <td><input type="text" name="kelas" class="input"></td>
                                    </tr>
                                    <tr>
                                        <td>Alamat:</td>
                                        <td><input type="text" name="alamat" class="input"></td>
                                    </tr>
                                    <tr>
                                        <td><input type="submit" value="Simpan" name="submit" class="btn btn-primary"></td>
                                    </tr>
                                </table>

                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

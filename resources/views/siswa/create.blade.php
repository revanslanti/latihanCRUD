@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">input Siswa</div>
                <a href="/siswa">Daftar Siswa</a>

                <div class="card-body">
                    @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                    @endif

                    @if ($errors->any())
                     <div class="alert alert-danger">
                       <ul>
                         @foreach ($errors->all() as $error)
                           <li>{{ $error }}</li>
                         @endforeach
                       </ul>
                     </div>
                    
                    @endif
                    <form action="/siswa" method="post">
                        {!! csrf_field() !!}
                      <p>Nama</p><input type="text" name="nama" id="">
                      <p>kelas</p><input type="text" name="kelas" id="">
                      <p>alamat</p><input type="text" name="alamat" id="">
                      <hr>
                      <p><button type="submit">Simpan</button></p>
                  </form>
              </div>
          </div>
      </div>
  </div>
</div>
@endsection

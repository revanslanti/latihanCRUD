@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Edit Siswa</div>
                <a href="/siswa">Daftar Siswa</a>

                <div class="card-body">
                    @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                    @endif
                    <form action="/siswa/update/{{ $siswa -> id }}" method="post">
                        {!! csrf_field() !!}
                      <p>Nama</p><input type="text" name="nama" value="{{ $siswa -> nama }}">
                      <p>kelas</p><input type="text" name="kelas" value="{{ $siswa -> kelas }}">
                      <p>alamat</p><input type="text" name="alamat" value="{{ $siswa -> alamat }}">
                      
                      <input type="hidden" name="_method" value="PUT">
                      
                      <hr>
                      <p><button type="submit">Edit</button></p>
                  </form>
              </div>
          </div>
      </div>
  </div>
</div>
@endsection

@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Daftar Siswa</div>
                <a href="/siswa/create">Input Siswa</a>
                <div class="card-body">
                    @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                    @endif

                    <table border="1">
                        <thead>
                            <th>No.</th>
                            <th>Nama</th>
                            <th>Kelas</th>
                            <th>Alamat</th>
                            <th>Aksi</th>
                        </thead>
                        <tbody>
                            @foreach ($siswa as $key => $s)
                            <tr>
                                <td>{{ $key+1 }}</td>
                                <td>{{ $s -> nama }}</td>
                                <td>{{ $s -> kelas }}</td>
                                <td>{{ $s -> alamat }}</td>
                                <td>
                                    <a href="siswa/edit/{{ $s -> id }}">Edit</a>
                                    <a href="siswa/delete/{{ $s -> id }}">Hapus</a>
                                </td>

                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

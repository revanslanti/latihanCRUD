<h3>Daftar Guru</h3>
<table border="1">
	<thead>
		<tr>
			<th>No</th>
			<th>Nama</th>
			<th>NIP</th>
			<th>Aksi</th>
		</tr>
	</thead>
	<tbody>
		@foreach ($guru as $key => $g)
			<tr>
				<td>{{ $key+1 }}</td>
				<td>{{ $g->nama }}</td>
				<td>{{ $g->nip }}</td>
				<td>
					<a href="guru/{{ $g->id }}">Hapus</a>
					<a href="guru/{{ $g->id }}">Edit</a>
				</td>
			</tr>
		@endforeach
	</tbody>
</table>